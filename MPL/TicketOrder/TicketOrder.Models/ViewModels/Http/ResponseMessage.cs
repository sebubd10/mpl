﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,19,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,19,2021
 * (c) MetLife Bangladesh
 */

namespace TicketOrder.Models.ViewModels.Http
{
    public class ResponseMessage
    {
        public object ObjectId { get; set; }
        public object ResponseObj { get; set; }
        public int ResponseCode { get; set; }
        public string Message { get; set; }
    }
}

﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,25,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Sep,07,2021
 * (c) Datavanced LLC
 */

using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketOrder.Models.DTO
{
    public class AuditEntity<TCreatedBy, TUpdatedBy>
    {
        public TCreatedBy CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        public TUpdatedBy UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        public int Status { get; set; }
    }
}

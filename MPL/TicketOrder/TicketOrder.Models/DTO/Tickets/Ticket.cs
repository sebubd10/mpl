﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketOrder.Models.DTO.Tickets
{
    public class Ticket : AuditEntity<int, int>
    {
        [Key]
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        [MaxLength(120)]
        public string TicketCategory { get; set; }
        [MaxLength(120)]
        public string Venue { get; set; }
        public virtual ICollection<TicketOrder> TicketOrders { get; set; }
    }
}

﻿using TicketOrder.Models.DTO.Profile;
using System.ComponentModel.DataAnnotations;

namespace TicketOrder.Models.DTO.Tickets
{
    public class TicketOrder : AuditEntity<int, int>
    {
        [Key]
        public int Id { set; get; }
        public int TicketId { get; set; }
        public virtual Ticket Ticket { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public int Quantity { get; set; }
        public decimal TotalAmount { get; set; }
    }
}

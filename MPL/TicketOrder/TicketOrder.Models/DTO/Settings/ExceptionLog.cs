﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TicketOrder.Models.DTO.Auth
{
    public class ExceptionLog
    {
        [Key]
        [Required]
        public int Id { get; set; }
        public int ExceptionType { get; set; }
        public string Location { get; set; }
        public string Message { get; set; }
        public DateTime ExceptionTime { get; set; }
    }
}

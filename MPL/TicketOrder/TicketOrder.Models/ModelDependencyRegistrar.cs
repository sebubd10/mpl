﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,25,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,25,2021
 * (c) Datavanced LLC
 */

using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;

namespace TicketOrder.Models
{
    public static class ModelDependencyRegistrar
    {
        /// <summary>
        /// Register Mapper and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void ModelDependencyResolver(this IServiceCollection services)
        {
            
        }
    }
}

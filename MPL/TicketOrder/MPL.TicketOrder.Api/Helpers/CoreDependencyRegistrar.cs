﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,25,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,25,2021
 * (c) Datavanced LLC
 */


using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TicketOrder.Utils.Helpers;

namespace MPL.TicketOrder.Api.Helpers
{
    public static class HelperDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void AddHelperServiceDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IHttpRequestHelper, HttpRequestHelper>();
            services.AddScoped<AuthFilterAttribute>();

        }
    }
}

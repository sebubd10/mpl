﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.Net;
using System.Text;
using TicketOrder.DAL;

namespace MPL.TicketOrder.Api.Helper
{
    public static class ExceptionHandler
    {
        public static void UseApiExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    //if any exception then report it and log it
                    if (contextFeature != null)
                    {
                        var customDbContextFactory = app.ApplicationServices.GetRequiredService<ICustomDbContextFactory<MPLDbContext>>();
                        await using var dbContext = customDbContextFactory.CreateDbContext("DefaultConnection");
                        //Technical Exception for troubleshooting
                        //var logger = loggerFactory.CreateLogger("GlobalException");
                        //logger.LogError($"Something went wrong: {contextFeature.Error}");
                        SqlParameter[] sqlParams = new SqlParameter[]
                        {
                            new SqlParameter { ParameterName = "@file_stream", Value = Encoding.ASCII.GetBytes(contextFeature.Error.Message) ,SqlDbType = SqlDbType.VarBinary, Direction = ParameterDirection.Input},
                            new SqlParameter { ParameterName = "@name", Value = $"ERROR_{Guid.NewGuid()}" ,SqlDbType = SqlDbType.NVarChar, Direction = ParameterDirection.Input},
                        };
                        dbContext.Database.ExecuteSqlRaw("EXEC SP_SaveExceptionLog @file_stream, @name",sqlParams);
                        //Business exception - exit gracefully
                        await context.Response.WriteAsync(new 
                        {
                            ResponseCode = context.Response.StatusCode,
                            Message = "Something went wrongs.Please try again later"
                        }.ToString());
                    }
                });
            });
        }
    }
}

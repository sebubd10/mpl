﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MPL.TicketOrder.Api.Helpers;
using System;
using System.Threading.Tasks;
using TicketOrder.Features.CurciteBraker;
using TicketOrder.Features.TicketOrder;

namespace MPL.TicketOrder.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketOrderController : ControllerBase
    {
        private IMediator _mediator;
        private readonly IRetryPolicy _retryPolicy;
        public TicketOrderController(IMediator mediator, IRetryPolicy retryPolicy)
        {
            _mediator = mediator;
            _retryPolicy = retryPolicy;
        }
        [HttpPost("CreateTicketOrder")]
        public async Task<IActionResult> CreateTicketOrder(CreateTicketOrderCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));

        }

        [HttpPost("UpdateTicketOrder")]
        public async Task<IActionResult> UpdateTicketOrder(UpdateTicketOrderCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [HttpPost("DeleteTicketOrder")]
        public async Task<IActionResult> DeleteTicketOrder(DeleteTicketOrderCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [HttpGet("GetAllTicketOrder")]
        public async Task<IActionResult> GetAllTicketOrder()
        {
            return Ok(await _mediator.Send(new GetAllTicketOrderQueries()));
        }

        [HttpGet("GetTicketOrderDetails/{id}")]
        [ServiceFilter(typeof(AuthFilterAttribute))]
        public async Task<IActionResult> GetTicketOrderDetails(int id)
        {
            var query = new GetAllTicketOrderQueries();
            query.Expression = x => x.Id == id;
            query.Selector = x => x;

            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(query)));
        }
    }
}

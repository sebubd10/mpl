﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace TicketOrder.DAL
{
    public class CustomDbContextFactory<T> : ICustomDbContextFactory<T> where T : DbContext
    {
        public IConfiguration _configuration;
        public CustomDbContextFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public T CreateDbContext(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<T>();
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString(connectionString), opt =>
            {
                
            });
            return Activator.CreateInstance(typeof(T), optionsBuilder.Options) as T;
        }
    }

    public interface ICustomDbContextFactory<out T> where T : DbContext
    {
        T CreateDbContext(string connectionString);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TicketOrder.Repository;

namespace TicketOrder.DAL
{
    public static class DALDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void AddDBDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<MPLDbContext>(options =>
         options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
            services.AddSingleton<ICustomDbContextFactory<MPLDbContext>, CustomDbContextFactory<MPLDbContext>>();

        }
    }
}

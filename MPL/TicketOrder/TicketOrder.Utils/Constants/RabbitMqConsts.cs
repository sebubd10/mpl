﻿namespace TicketOrder.Utils.Constants
{
    public class RabbitMqConsts
    {
        public const string RabbitMqRootUri = "rabbitmq://localhost";
        public const string RabbitMqUriForQuery = "rabbitmq://localhost/queryEndPonit";
        public const string RabbitMqUriForCommand = "rabbitmq://localhost/commandEndPonit";
        public const string UserName = "guest";
        public const string Password = "guest";
        public const string QueryEndPoint = "queryEndPonit";
        public const string commandEndPoint = "commandEndPonit";
        //public const string NotificationServiceQueue = "notification.service";
    }
}

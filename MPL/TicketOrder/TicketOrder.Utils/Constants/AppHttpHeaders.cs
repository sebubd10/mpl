﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,19,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,19,2021
 * (c) Datavanced LLC
 */

namespace TicketOrder.Utils.Constants
{
    public class AppHttpHeaders
    {
        public const string Token = "Authorization";
        public const string AuthenticationSchema = "Bearer ";
    }
}

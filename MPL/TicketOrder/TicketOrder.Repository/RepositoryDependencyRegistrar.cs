﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Dec,07,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,25,2021
 * (c) Datavanced LLC
 */

using Microsoft.Extensions.DependencyInjection;

namespace TicketOrder.Repository
{
    public static class RepositoryDependencyRegistrar
    {
        /// <summary>
        /// Register Repository and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void RepositoryDependencyResolver(this IServiceCollection services)
        {
            //repositories
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
        }
    }
}

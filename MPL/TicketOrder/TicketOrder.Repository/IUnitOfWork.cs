﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,25,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,25,2021
 * (c) Datavanced LLC
 */

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TicketOrder.Repository
{
    public interface IUnitOfWork<TContext> : IDisposable where TContext : DbContext, new()
    {
        TContext DbContext { get; }
        int SaveChanges(bool acceptAllChangesOnSuccess);
        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess);
        int SaveChanges();
        Task<int> SaveChangesAsync();
        IGenericRepository<T> Repository<T>() where T : class, new();
        List<T> RawSqlQuery<T>(string query) where T : class, new();
        Task<List<T>> RawSqlQueryAsync<T>(string query) where T : class, new();
        Task<T> RawSqlScalarAsync<T>(string query);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TicketOrder.Models.ViewModels.Http;
using TicketOrder.Repository;
using TicketOrder.Utils.Enums;
using TicketOrder.Utils.Extensions;

namespace TicketOrder.Features
{
    public class GenericServices<TDbContext, TEntity> : IGenericServices<TDbContext, TEntity>
        where TDbContext : DbContext, new()
        where TEntity : class, new()
    {
        private readonly IUnitOfWork<TDbContext> _unitOfWork;
        public GenericServices(IUnitOfWork<TDbContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public virtual async Task<ResponseMessage> Save(TEntity entity, List<(string message, Expression<Func<TEntity, bool>> expression)> validations = null)
        {
            validations ??= new List<(string message, Expression<Func<TEntity, bool>> expression)>();
            var responseMessage = new ResponseMessage();
            try
            {
                foreach (var (message, expression) in validations)
                {
                    if (expression != null && await _unitOfWork.Repository<TEntity>().IsExistAsync(expression))
                    {
                        responseMessage.Message = message;
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                        return responseMessage;
                    }
                }

                await _unitOfWork.Repository<TEntity>().InsertAsync(entity);
                var responseCode = await _unitOfWork.SaveChangesAsync() > 0 ? (int)AppEnums.ResponseCode.Success : (int)AppEnums.ResponseCode.Failed;
                responseMessage.ResponseCode = responseCode;
                responseMessage.ResponseObj = entity;
            }
            catch (Exception ex)
            {
                responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
            }
            return responseMessage;
        }

        public virtual async Task<ResponseMessage> Update(TEntity entity, List<(string message, Expression<Func<TEntity, bool>> expression)> validations = null,
            List<string> avoidProperties = null)
        {
            avoidProperties ??= new List<string>();
            validations ??= new List<(string message, Expression<Func<TEntity, bool>> expression)>();
            var responseMessage = new ResponseMessage();
            try
            {
                //https://stackoverflow.com/questions/35231897/get-name-and-value-of-static-class-properties-using-expression-trees
                foreach (var (message, expression) in validations)
                {
                    if (expression != null && await _unitOfWork.Repository<TEntity>().IsExistAsync(expression))
                    {
                        responseMessage.Message = message;
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                        return responseMessage;
                    }
                }

                _unitOfWork.Repository<TEntity>().Update(entity, avoidProperties.ToArray());
                responseMessage.ResponseCode = await _unitOfWork.SaveChangesAsync() > 0 ? (int)AppEnums.ResponseCode.Success : (int)AppEnums.ResponseCode.Failed;
                responseMessage.ResponseObj = entity;
            }
            catch (Exception ex)
            {
                throw;
            }
            return responseMessage;
        }

        public virtual async Task<ResponseMessage> UpdateWithAcceptedProperties(TEntity entity, List<(string message, Expression<Func<TEntity, bool>> expression)> validations = null,
            List<string> acceptedProperties = null)
        {
            acceptedProperties ??= new List<string>();
            validations ??= new List<(string message, Expression<Func<TEntity, bool>> expression)>();
            var responseMessage = new ResponseMessage();
            try
            {
                foreach (var (message, expression) in validations)
                {
                    if (expression != null && await _unitOfWork.Repository<TEntity>().IsExistAsync(expression))
                    {
                        responseMessage.Message = message;
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                        return responseMessage;
                    }
                }
                _unitOfWork.Repository<TEntity>().UpdateWithAcceptedProperties(entity, acceptedProperties.ToArray());
                var responseCode = await _unitOfWork.SaveChangesAsync() > 0 ? (int)AppEnums.ResponseCode.Success : (int)AppEnums.ResponseCode.Failed;
                responseMessage.ResponseCode = responseCode;
                responseMessage.ResponseObj = entity;
                if (responseCode == (int)AppEnums.ResponseCode.Success)
                {
                    responseMessage.Message = "Updated Successfully";
                }
                else
                {
                    responseMessage.Message = "Can't Update";
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return responseMessage;
        }

        public virtual async Task<ResponseMessage> Delete(TEntity entity, Expression<Func<TEntity, bool>> validation = null)
        {
            var responseMessage = new ResponseMessage();
            try
            {
                if (validation != null && !await _unitOfWork.Repository<TEntity>().IsExistAsync(validation))
                {
                    responseMessage.Message = "Record doesn't Exist !";
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                }
                else
                {
                    _unitOfWork.Repository<TEntity>().Delete(entity);
                    if (await _unitOfWork.SaveChangesAsync() > 0)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                        responseMessage.ResponseObj = true;
                    }
                    else
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                        responseMessage.ResponseObj = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return responseMessage;
        }

        public virtual async Task<ResponseMessage> DeleteRange(IEnumerable<TEntity> entities, Expression<Func<TEntity, bool>> validation = null)
        {
            var responseMessage = new ResponseMessage();
            try
            {
                if (validation != null && !await _unitOfWork.Repository<TEntity>().IsExistAsync(validation))
                {
                    responseMessage.Message = "Record doesn't Exist !";
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                }
                else
                {
                    _unitOfWork.Repository<TEntity>().DeleteRange(entities);
                    if (await _unitOfWork.SaveChangesAsync() > 0)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                        responseMessage.ResponseObj = true;
                    }
                    else
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                        responseMessage.ResponseObj = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return responseMessage;
        }

        public async Task<int> GetListCount(Expression<Func<TEntity, bool>> expression = null)
        {
            try
            {
                return expression is { }
                    ? await _unitOfWork.Repository<TEntity>().CountAsync(expression)
                    : await _unitOfWork.Repository<TEntity>().CountAsync();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public virtual async Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> expression = null,
            Expression<Func<TEntity, TEntity>> selector = null)
        {
            try
            {
                if (selector != null)
                {
                    return expression != null ? await _unitOfWork.Repository<TEntity>().GetAsync(selector: selector, expression) :
                     await _unitOfWork.Repository<TEntity>().GetAsync(selector: selector);
                }
                else
                {
                    return expression != null ? await _unitOfWork.Repository<TEntity>().GetAsync(predicate: expression) :
                     await _unitOfWork.Repository<TEntity>().GetAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<TEntity> GetDetailsAsync(Expression<Func<TEntity, bool>> expression = null,
            Expression<Func<TEntity, TEntity>> selector = null)
        {
            try
            {
                if (selector != null)
                {
                    return expression != null ? await _unitOfWork.Repository<TEntity>().GetFirstOrDefaultAsync(selector: selector, expression) :
                     await _unitOfWork.Repository<TEntity>().GetFirstOrDefaultAsync(selector: selector);
                }
                else
                {
                    return expression != null ? await _unitOfWork.Repository<TEntity>().GetFirstOrDefaultAsync(predicate: expression) :
                     await _unitOfWork.Repository<TEntity>().GetFirstOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IEnumerable<TEntity>> GetListWithPaginationAsync<TKey>(Expression<Func<TEntity, bool>> expression = null, Expression<Func<TEntity, TEntity>> selector = null, int skip = 0, int take = 0,
            Expression<Func<TEntity, TKey>> keySelector = null,
            bool descending = true)
        {
            try
            {
                if (expression == null)
                {
                    return selector is { } ? await _unitOfWork.DbContext.Set<TEntity>().Select(selector).AddOrdering(keySelector, descending).Skip(skip).Take(take).ToListAsync() :
                        await _unitOfWork.DbContext.Set<TEntity>().AddOrdering(keySelector, descending).Skip(skip).Take(take).ToListAsync();
                }

                return selector is { } ? await _unitOfWork.DbContext.Set<TEntity>().Where(expression).Select(selector).AddOrdering(keySelector, descending).Skip(skip).Take(take).ToListAsync() :
                    await _unitOfWork.DbContext.Set<TEntity>().Where(expression).AddOrdering(keySelector, descending).Skip(skip).Take(take).ToListAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

    public interface IGenericServices<TDbContext, TEntity>
        where TDbContext : DbContext, new()
        where TEntity : class, new()
    {
        Task<ResponseMessage> Save(TEntity entity, List<(string message, Expression<Func<TEntity, bool>> expression)> validations = null);
        Task<ResponseMessage> Update(TEntity entity, List<(string message, Expression<Func<TEntity, bool>> expression)> validations = null,
            List<string> avoidProperties = null);
        Task<ResponseMessage> UpdateWithAcceptedProperties(TEntity entity, List<(string message, Expression<Func<TEntity, bool>> expression)> validations = null,
            List<string> acceptedProperties = null);
        Task<ResponseMessage> Delete(TEntity entity, Expression<Func<TEntity, bool>> validation = null);
        Task<ResponseMessage> DeleteRange(IEnumerable<TEntity> entities, Expression<Func<TEntity, bool>> validation = null);
        Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> expression = null,
            Expression<Func<TEntity, TEntity>> selector = null);

        Task<IEnumerable<TEntity>> GetListWithPaginationAsync<TKey>(Expression<Func<TEntity, bool>> expression = null, Expression<Func<TEntity, TEntity>> selector = null, int skip = 0, int take = 0,
            Expression<Func<TEntity, TKey>> keySelector = null,
            bool descending = true);
        Task<TEntity> GetDetailsAsync(Expression<Func<TEntity, bool>> expression = null,
            Expression<Func<TEntity, TEntity>> selector = null);

        Task<int> GetListCount(Expression<Func<TEntity, bool>> expression = null);
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TicketOrder.Features.BusService;
using TicketOrder.Features.CurciteBraker;
using TicketOrder.Features.TicketOrder;

namespace TicketOrder.Features
{
    public static class FeaturesDependencyRegistrar
    {
        /// <summary>
        /// Register Repository and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void FeaturesDependencyResolver(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddTransient<ICutomerService, CutomerService>();
            services.AddTransient<ITicketOrderServie, TicketOrderService>();
            //services.AddTransient<IUserTypeService, UserTypeService>();
            services.AddTransient<IRetryPolicy, RetryPolicy>();
            services.AddTransient<IBusService, BusServices>();
        }
    }

}

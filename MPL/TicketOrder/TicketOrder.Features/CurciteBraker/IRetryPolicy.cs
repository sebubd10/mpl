﻿using Polly.Retry;
using System;

namespace TicketOrder.Features.CurciteBraker
{
    public interface IRetryPolicy
    {
        AsyncRetryPolicy GetRetryPolicy(int retryCount, TimeSpan timeSpan);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketOrder.DAL;
using TicketOrder.Repository;

namespace TicketOrder.Features.TicketOrder
{
    public class TicketOrderService : GenericServices<MPLDbContext, Models.DTO.Tickets.TicketOrder>, ITicketOrderServie
    {
        private readonly IUnitOfWork<MPLDbContext> _unitOfWork;
        public TicketOrderService(IUnitOfWork<MPLDbContext> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
    public interface ITicketOrderServie : IGenericServices<MPLDbContext, Models.DTO.Tickets.TicketOrder>
    {

    }
}

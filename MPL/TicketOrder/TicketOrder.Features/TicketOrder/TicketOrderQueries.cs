﻿using MediatR;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using TicketOrder.Models.ViewModels.Http;
using TicketOrder.Utils.Enums;

namespace TicketOrder.Features.TicketOrder
{
    /// <summary>
    /// Get all Ticket Order Query Request works via MediatR
    /// </summary>
    public class GetAllTicketOrderQueries : IRequest<ResponseMessage>
    {
        public Expression<Func<Models.DTO.Tickets.TicketOrder, bool>> Expression { get; set; }
        public Expression<Func<Models.DTO.Tickets.TicketOrder, Models.DTO.Tickets.TicketOrder>> Selector { get; set; }
        /// <summary>
        /// Get All Ticket Order Query Handler Fires when the MediatR Request for Get all ticket order
        /// </summary>
        public class GetAllTicketOrderQueriesHandler : IRequestHandler<GetAllTicketOrderQueries, ResponseMessage>
        {
            private readonly ITicketOrderServie _ticketOrderServie;
            public GetAllTicketOrderQueriesHandler(ITicketOrderServie ticketOrderServie)
            {
                _ticketOrderServie = ticketOrderServie;
            }
            /// <summary>
            /// Excutes the Get list of ticket orders Method and also activates the poly retry policy
            /// </summary>
            /// <param name="query"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(GetAllTicketOrderQueries query, CancellationToken cancellationToken)
            {
                ResponseMessage responseMessage = new ResponseMessage();
                try
                {
                    var response = await _ticketOrderServie.GetListAsync(query.Expression, query.Selector);
                    responseMessage.ResponseObj = response;
                    if (response != null)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                    }
                    else
                    {
                        responseMessage.Message = "Data not found";
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                    }
                }
                catch (Exception ex)
                {
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                    responseMessage.Message = ex.Message;
                }
                return responseMessage;
            }
        }

        /// <summary>
        /// Get details of a Ticket Order Query Request works via MediatR
        /// </summary>


        public class TicketOrderDetailsQuery : IRequest<ResponseMessage>
        {
            public Expression<Func<Models.DTO.Tickets.TicketOrder, bool>> Expression { get; set; }
            public Expression<Func<Models.DTO.Tickets.TicketOrder, Models.DTO.Tickets.TicketOrder>> Selector { get; set; }

            public class TicketDetailsQueryHandler : IRequestHandler<TicketOrderDetailsQuery, ResponseMessage>
            {

                private readonly ITicketOrderServie _ticketOrderServie;
                public TicketDetailsQueryHandler(ITicketOrderServie ticketOrderServie)
                {
                    _ticketOrderServie = ticketOrderServie;
                }
                public async Task<ResponseMessage> Handle(TicketOrderDetailsQuery query, CancellationToken cancellationToken)
                {
                    ResponseMessage responseMessage = new ResponseMessage();
                    try
                    {

                        responseMessage.ResponseObj = await _ticketOrderServie.GetDetailsAsync(query.Expression, query.Selector);

                        if (responseMessage.ResponseObj != null)
                        {
                            responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                        }
                        else
                        {
                            responseMessage.Message = "Data not found.";
                            responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                        }

                    }
                    catch (Exception ex)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                        responseMessage.Message = ex.Message;
                    }

                    return responseMessage;
                }
            }
        }
    }
}

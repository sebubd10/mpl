﻿using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using TicketOrder.Features.BusService;
using TicketOrder.Models.RequestModels.Tickets;
using TicketOrder.Models.ViewModels.Http;
using TicketOrder.Utils.Constants;
using TicketOrder.Utils.Enums;
using Newtonsoft.Json;
using TicketOrder.Features.CurciteBraker;
using BusService.Model;

namespace TicketOrder.Features.TicketOrder
{
    /// <summary>
    /// Create Ticket Order Command Request works via MediatR
    /// </summary>
    public class CreateTicketOrderCommand : TicketOrderRequest, IRequest<ResponseMessage>
    {
        /// <summary>
        /// Create Ticket Order Command Handler Fires when the MediatR Request for Create Ticket Order
        /// </summary>
        public class CreateTicketOrderCommandHandler : IRequestHandler<CreateTicketOrderCommand, ResponseMessage>
        {
            private readonly ITicketOrderServie _ticketOrderServie;
            private readonly IBusService _busService;
            private readonly IMapper _mapper;
            private readonly IRetryPolicy _retryPolicy;
            public CreateTicketOrderCommandHandler(ITicketOrderServie ticketOrderServie, IMapper mapper, IBusService busService, IRetryPolicy retryPolicy)
            {
                _ticketOrderServie = ticketOrderServie;
                _mapper = mapper;
                _busService = busService;
                _retryPolicy = retryPolicy;
            }

            /// <summary>
            /// Excutes the Ticket Order Save Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>

            public async Task<ResponseMessage> Handle(CreateTicketOrderCommand command, CancellationToken cancellationToken)
            {
                try
                {
                    var ticketOrderModel = _mapper.Map<Models.DTO.Tickets.TicketOrder>(command);
                    var result = await _ticketOrderServie.Save(ticketOrderModel);
                    if (result.ResponseCode == (int)AppEnums.ResponseCode.Success)
                    {

                        var transporter = new Transporter() { Data = JsonConvert.SerializeObject(result.ResponseObj) };
                        var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
                        await retryPolicy.ExecuteAsync(async () => await _busService.PublishModel(transporter, RabbitMqConsts.RabbitMqUriForCommand));

                    }
                    return result;
                }
                catch (Exception ex)
                {

                    throw;
                }

            }


        }
    }
    /// <summary>
    /// Update Ticket Order Command Request works via MediatR
    /// </summary>
    public class UpdateTicketOrderCommand : TicketOrderRequest, IRequest<ResponseMessage>
    {
        public class UpdateTicketCommandHandler : IRequestHandler<UpdateTicketOrderCommand, ResponseMessage>
        {
            private readonly ITicketOrderServie _ticketOrderServie;
            readonly IMapper _mapper;
            /// <summary>
            /// Update Ticket Order Command Handler Fires when the MediatR Request for Update Ticket Order
            /// </summary>
            public UpdateTicketCommandHandler(ITicketOrderServie ticketOrderServie, IMapper mapper)
            {
                _ticketOrderServie = ticketOrderServie;
                _mapper = mapper;
            }

            /// <summary>
            /// Excutes the Ticket Update Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>

            public async Task<ResponseMessage> Handle(UpdateTicketOrderCommand command, CancellationToken cancellationToken)
            {
                var ticketOrderModel = _mapper.Map<Models.DTO.Tickets.TicketOrder>(command);
                return await _ticketOrderServie.Update(ticketOrderModel);
            }
        }
    }
    /// <summary>
    /// Delete Ticket Order Command Request works via MediatR
    /// </summary>
    public class DeleteTicketOrderCommand : TicketOrderRequest, IRequest<ResponseMessage>
    {
        /// <summary>
        /// Delete Ticket Order Command Handler Fires when the MediatR Request for Delete Ticket Order
        /// </summary>
        public class DeleteTicketOrderCommandHandler : IRequestHandler<DeleteTicketOrderCommand, ResponseMessage>
        {
            private readonly ITicketOrderServie _ticketOrderServie;
            readonly IMapper _mapper;
            public DeleteTicketOrderCommandHandler(ITicketOrderServie ticketServie, IMapper mapper)
            {
                _ticketOrderServie = ticketServie;
                _mapper = mapper;
            }
            /// <summary>
            /// Excutes the Ticket Delete Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>

            public async Task<ResponseMessage> Handle(DeleteTicketOrderCommand command, CancellationToken cancellationToken)
            {
                var ticketOrderModel = _mapper.Map<Models.DTO.Tickets.TicketOrder>(command);
                return await _ticketOrderServie.Delete(ticketOrderModel);
            }
        }
    }
}
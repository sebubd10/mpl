﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MPL.Core;
using MPL.Features.Cutomer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Api.Controllers.Customer
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private IMediator _mediator;
        public CustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("CreateCustomer")]
        public async Task<IActionResult> CreateCustomer(CreateCutomerCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpPost("UpdateCustomer")]
        public async Task<IActionResult> UpdateCustomer(UpdateCustomerCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpPost("DeleteCustomer")]
        public async Task<IActionResult> DeleteCustomer(DeleteCustomerCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [ServiceFilter(typeof(AuthFilterAttribute))]
        [HttpGet("GetAllCustomer")]
        public async Task<IActionResult> GetAllCustomer()
        {
            return Ok(await _mediator.Send(new GetAllCustomerQueries()));
        }
        [HttpGet("GetCustomerDetails/{id}")]
        public async Task<IActionResult> GetCustomerDetails(int id)
        {

            var query = new CustomerDetailsQuery();
            query.Expression = x => x.Id == id;
            query.Selector = x => x;
            return Ok(await _mediator.Send(query));
        }

    }
}

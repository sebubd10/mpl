﻿using CoreApi.Features.CurciteBraker;
using CoreApi.Features.Ticket;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace MPL.Api.Controllers.Ticket
{
    [Route("api/ticket")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private IMediator _mediator;
        private readonly IRetryPolicy _retryPolicy;

        public TicketController(IMediator mediator, IRetryPolicy retryPolicy)
        {
            _mediator = mediator;
            _retryPolicy = retryPolicy;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateTicket(CreateTicketCommands command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateTicket(UpdateTicketCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [HttpPost("delete")]
        public async Task<IActionResult> DeleteTicket(DeleteTicketCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [HttpGet("alltickets")]
        public async Task<IActionResult> GetAllTicket()
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(new GetAllTicketQueries())));
        }

        [HttpGet("getticket/{id}")]
        public async Task<IActionResult> GetDetailsTicket(int id)
        {
            var query = new TicketDetailsQuery();
            query.Expression = x => x.Id == id;
            query.Selector = x => x;

            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(query)));
        }
    }
}

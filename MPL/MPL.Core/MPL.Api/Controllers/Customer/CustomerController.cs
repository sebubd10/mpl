﻿using CoreApi.Features.CurciteBraker;
using CoreApi.Features.Cutomer;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MPL.Api.Helper;
using System;
using System.Threading.Tasks;

namespace MPL.Api.Controllers.Customer
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private IMediator _mediator;
        private readonly IRetryPolicy _retryPolicy;

        public CustomerController(IMediator mediator, IRetryPolicy retryPolicy)
        {
            _mediator = mediator;
            _retryPolicy = retryPolicy;
        }


        [HttpPost("CreateCustomer")]
        public async Task<IActionResult> CreateCustomer(CreateCutomerCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [HttpPost("UpdateCustomer")]
        public async Task<IActionResult> UpdateCustomer(UpdateCustomerCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [HttpPost("DeleteCustomer")]
        public async Task<IActionResult> DeleteCustomer(DeleteCustomerCommand command)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(command)));
        }

        [ServiceFilter(typeof(AuthFilterAttribute))]
        [HttpGet("GetAllCustomer")]
        public async Task<IActionResult> GetAllCustomer()
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(new GetAllCustomerQueries())));
        }

        [HttpGet("GetCustomerDetails/{id}")]
        public async Task<IActionResult> GetCustomerDetails(int id)
        {

            var query = new CustomerDetailsQuery();
            query.Expression = x => x.Id == id;
            query.Selector = x => x;

            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(query)));
        }

    }
}

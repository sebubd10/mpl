﻿using CoreApi.Features.CurciteBraker;
using CoreApi.Features.Cutomer;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Api.Controllers.Auth
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IMediator _mediator;
        private readonly IRetryPolicy _retryPolicy;

        public AuthController(IMediator mediator, IRetryPolicy retryPolicy)
        {
            _mediator = mediator;
            _retryPolicy = retryPolicy;
        }


        [HttpPost("Login")]
        public async Task<IActionResult> Login(CustomerAuthQuery query)
        {
            var retryPolicy = _retryPolicy.GetRetryPolicy(retryCount: 3, timeSpan: TimeSpan.FromSeconds(2));
            return Ok(await retryPolicy.ExecuteAsync(async () => await _mediator.Send(query)));
        }

    }
}

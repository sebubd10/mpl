﻿using AutoMapper;
using BusService.Model;
using CoreApi.Features.Ticket;
using CoreApi.Models.DTO.Tickets;
using MassTransit;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MPL.Consumer
{
    public class TicketConsumer : IConsumer<Transporter>
    {
        private IMediator _mediator;
        private readonly IMapper _mapper;
        public TicketConsumer(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }
        public async Task Consume(ConsumeContext<Transporter> context)
        {
            //var command = _mapper.Map<UpdateTicketCommand>(ProcessInventoryData(context.Message));
            var data = JsonConvert.DeserializeObject<CoreApi.Models.DTO.Tickets.TicketOrder>(context.Message?.Data
                .ToString());
            //var data = (CoreApi.Models.DTO.Tickets.TicketOrder)(context.Message.Data);

            try
            {
                var updateCommand =await ProcessInventoryData(data);

                var res = await _mediator.Send(updateCommand);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private async Task<UpdateTicketCommand> ProcessInventoryData(CoreApi.Models.DTO.Tickets.TicketOrder ticketOrder)
        {
            try
            {
                var query = new TicketDetailsQuery();
                query.Expression = x => x.Id == ticketOrder.TicketId;
                query.Selector = x => x;
                var result =await _mediator.Send(query);

                var ticket = _mapper.Map<Ticket>(result.ResponseObj);

                ticket.Quantity = ticket.Quantity - ticketOrder.Quantity;

                var tickets = new UpdateTicketCommand()
                {
                    CreatedBy = ticket.CreatedBy,
                    CreatedDate = ticket.CreatedDate,
                    Id = ticket.Id,
                    Price = ticket.Price,
                    Quantity = ticket.Quantity,
                    Status = ticket.Status,
                    TicketCategory = ticket.TicketCategory,
                    UpdatedBy = ticket.UpdatedBy,
                    UpdatedDate = ticket.UpdatedDate,
                    Venue = ticket.Venue
                };

                return tickets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using CoreApi.Utils.Helpers;
using CoreApi.Utils.Constants;
using CoreApi.Utils.Extensions;

namespace MPL.Api.Helper
{
    //public class AuthFilterAttribute
    //{
    //}

    /// <summary>
    /// AuthFilterAttribute is inherited by ActionFilterAttribute class, you can apply this attribute class for a controller action or entire controller to filter the request. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class AuthFilterAttribute : ActionFilterAttribute
    {
        private IConfiguration _config;
        private readonly IHttpRequestHelper _httpRequestHelper;
        private int _userId;
        private int _userTypeId;
        private bool _checkPermissionBtUserType;
        private (object accessApi, bool isPermitted) _apiDetailsWithPermission;

        public AuthFilterAttribute(IConfiguration config, IHttpRequestHelper httpRequestHelper)
        {
            _config = config;
            _httpRequestHelper = httpRequestHelper;
        }
        public async override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            try
            {
                
                //Batch(1): Checking the Allow Anonymous Attribute has been used by action or not
                if (context.Filters.Contains(new AllowAnonymousAttribute()))
                {
                    base.OnActionExecuting(context);
                    return;
                }
                //Batch(2): Is token pass by http header
                if (context.HttpContext.Request.Headers.ContainsKey(AppHttpHeaders.Token))
                {
                    var handler = new JwtSecurityTokenHandler();
                    var tokenValue = GetTokenFromRequest(context);
                    var token = handler.ReadToken(tokenValue) as JwtSecurityToken;
                    //var expireDate = Convert.ToDateTime(token.Claims.First(claim => claim.Type == JwtClaims.ExpiresDate).Value);
                    var expireDate = Convert.ToDateTime(context.HttpContext.Request.GetTokenValue<string>(JwtClaims.ExpiresDate));
                    //_userId = context.HttpContext.Request.GetTokenValue<int>(JwtClaims.UserId);
                    //_userTypeId = context.HttpContext.Request.GetTokenValue<int>(JwtClaims.UserTypeId);

                    //batch(3): Checking organization from token and assign to http header for current request lifetime

                    //Batch(4): Checking Get request and token expire time
                    if (context.HttpContext.Request.Method == WebRequestMethods.Http.Get)
                        {
                            if (expireDate < DateTime.UtcNow)
                            {
                                context.Result = new UnauthorizedResult();
                            }
                        }
                        else
                        {

                        }
                    
                }
                else
                {
                    context.Result = new NotFoundResult();
                }
            }
            catch (Exception ex)
            {
                context.Result = new BadRequestResult();
            }
            await base.OnActionExecutionAsync(context, next);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {


            
        }

        private string GetTokenFromRequest(ActionExecutingContext context)
        {
            string fullToken = context.HttpContext.Request.Headers[AppHttpHeaders.Token];
            return fullToken.Replace(AppHttpHeaders.AuthenticationSchema, "");
        }
    }
}

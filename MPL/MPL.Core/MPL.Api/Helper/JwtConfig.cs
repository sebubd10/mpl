﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPL.Api.Helper
{
    public static class JwtConfig
    {
        public static void AddJwt(this IServiceCollection services, IConfiguration configuration)
        {
            //var appSettings = configuration.GetSection("AppSettings").Get<AppConfig>();
            //var appSettings = configuration.GetSection("AppSettings").Get<AppConfig>();

            //var key = Encoding.ASCII.GetBytes("jhytr4433!@#$");
            var key = Encoding.ASCII.GetBytes(configuration.GetSection("Jwt:TokenKey").Get<string>());
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }
    }
}

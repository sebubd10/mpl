﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using AutoMapper;
using CoreApi.DAL;
using CoreApi.Features.Cutomer;
using CoreApi.Features.Ticket;
using CoreApi.Features.Auth.UserType;
using CoreApi.Features.CurciteBraker;
using CoreApi.Features.BusService;
using CoreApi.Utils;
using CoreApi.Utils.Helpers;

namespace MPL.Api.Helper
{
    public static class DependencyConfig
    {
        //public static void AddDBDependencies(this IServiceCollection services, IConfiguration configuration)
        //{

        //    services.AddDbContext<MPLDbContext>(options =>
        //    options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        //    //services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
        //    //services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
        //    services.AddSingleton<ICustomDbContextFactory<MPLDbContext>, CustomDbContextFactory<MPLDbContext>>();


        //}


        //public static void AddServiceDependencies(this IServiceCollection services, IConfiguration configuration)
        //{
        //    services.AddTransient<ICutomerService, CutomerService>();
        //    services.AddTransient<ITicketServie, TicketService>();
        //    services.AddTransient<IUserTypeService, UserTypeService>();
        //    services.AddTransient<IRetryPolicy, RetryPolicy>();
        //    services.AddTransient<IBusService, BusService>();
            
        //}


        public static void AddCommonDependencies(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddCors();
            services.AddControllers();


            var mapperConfig = new MapperConfiguration(mc =>
            {
                //mc.AddProfile(new MappingProfile());
            });
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddCors(options =>
            {
                options.AddPolicy(name: "CorsPolicy",
                                  builder =>
                                  {
                                      builder.AllowAnyMethod()
                                  .AllowAnyHeader()
                                  .AllowCredentials()
                                  .WithOrigins("https://localhost:44346/", "http://localhost:4200", "https://passioncare.datavanced.com");
                                  });
            });
            services.AddMvc(options => options.EnableEndpointRouting = false)
                .AddNewtonsoftJson(o =>
                {
                    o.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    o.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;
                    o.UseCamelCasing(processDictionaryKeys: true);
                });
            services.AddHttpClient();
            //services.Configure<AppConfig>(configuration.GetSection("Profile"));
            services.AddScoped<IHttpRequestHelper, HttpRequestHelper>();
            services.AddScoped<AuthFilterAttribute>();
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                                  \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                },
                                Scheme = "oauth2",
                                Name = "Bearer",
                                In = ParameterLocation.Header,

                            },
                            new List<string>()
                        }
                    });
                c.CustomSchemaIds(i => i.FullName);
            });

          

            services.AddHttpContextAccessor();
        }

    }
}

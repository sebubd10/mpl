﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,25,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,25,2021
 * (c) Datavanced LLC
 */

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http.Connections;

namespace MPL.Api.Helper
{
    /// <summary>
    /// Represents extensions of IApplicationBuilder
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Configure the application HTTP request pipeline
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public static void ConfigureRequestPipeline(this IApplicationBuilder application, Microsoft.AspNetCore.Hosting.IWebHostEnvironment env)
        {
            // Make sure the CORS middleware is ahead of SignalR.
            application.UseCors("CorsPolicy");

            application.UseSwagger();

            application.UseRouting();
            application.UseStaticFiles();
            application.UseAuthorization();
            application.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            application.UseMvc();
        }
    }
}

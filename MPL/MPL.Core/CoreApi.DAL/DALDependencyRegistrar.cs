﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,25,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,25,2021
 * (c) Datavanced LLC
 */



using CoreApi.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CoreApi.DAL
{
    public static class DALDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void AddDBDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<MPLDbContext>(options =>
         options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
            services.AddSingleton<ICustomDbContextFactory<MPLDbContext>, CustomDbContextFactory<MPLDbContext>>();

        }
    }
}

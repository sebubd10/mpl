﻿using Microsoft.Extensions.DependencyInjection;

namespace CoreApi.Repository
{
    public static class RepositoryDependencyRegistrar
    {
        /// <summary>
        /// Register Repository and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void RepositoryDependencyResolvers(this IServiceCollection services)
        {
            //repositories
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
        }
    }
}

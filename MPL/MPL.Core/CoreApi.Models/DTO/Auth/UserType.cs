﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,25,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,26,2021
 * (c) Datavanced LLC
 */

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreApi.Models.DTO.Auth
{
    public class UserType: AuditEntity<int,int>
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [MaxLength(120)]
        public string UserTypeName { get; set; }
        public virtual ICollection<SystemUser> SystemUsers { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}

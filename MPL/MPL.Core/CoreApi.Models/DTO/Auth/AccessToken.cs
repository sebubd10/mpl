﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApi.Models.DTO.Auth
{
    public class AccessToken
    {
        [Key]
        [Required]
        public int AccessTokenID { get; set; }
   
        public int UserId { get; set; }

        public string Token { get; set; }
 
        public DateTime IssuedOn { get; set; }

        public DateTime ExpiresOn { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreApi.Models.DTO.Auth
{
    public class SystemUser : AuditEntity<int, int>
    {
        [Required]
        [Key]
        public int UserID { get; set; }
        [Required]
        [MaxLength(50)]
        public string UserName { get; set; }
        [ForeignKey("UserTypeId")]
        public virtual UserType UserType { get; set; }
        //[Required]
        public int UserTypeId { get; set; }
        //[Required]
        //[MaxLength(150)]
        public string FullName { get; set; }
        [MaxLength(70)]
        public string Email { get; set; }
        [MaxLength(30)]
        public string ContactNo { get; set; }
        //[Required]
        //[MaxLength(100)]
        [NotMapped]
        public string OldPassword { get; set; }
        public string Password { get; set; }
        [MaxLength(200)]
        public string Photo { get; set; }
        [Column(TypeName ="date")]
        public DateTime Birthday { get; set; }

        [NotMapped]
        public IFormFile file { get; set; }
        [NotMapped]
        public virtual List<UserType> lstUserType { get; set; }
    }
}

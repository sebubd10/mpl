﻿using CoreApi.Models.DTO.Tickets;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreApi.Models.DTO.Profile
{
    public class Customer:AuditEntity<int,int>
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(120)]
        public string FullName { get; set; }
        [MaxLength(250)]
        public string Address { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(20)]
        public string MobileNumber { get; set; }
        [MaxLength(120)]
        public string Password { get; set; }

        public virtual ICollection<TicketOrder> TicketOrders { get; set; }

    }
}

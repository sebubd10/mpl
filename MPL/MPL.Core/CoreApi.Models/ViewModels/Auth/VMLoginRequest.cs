﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,19,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,19,2021
 * (c) Datavanced LLC
 */

namespace CoreApi.Models.ViewModels.Auth
{
    public class VMLoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Organization { get; set; }
    }
}

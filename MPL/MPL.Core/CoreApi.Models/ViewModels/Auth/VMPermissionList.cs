﻿
namespace CoreApi.Models.ViewModels.Auth
{
    public class VMPermissionList
    {
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        public int PermissionUserTypeId { get; set; }
        public int PermissionID { get; set; }
        public int PermissionStatus { get; set; }
        public int PermissionAccessApiId { get; set; }
        public int AccessApiId { get; set; }
        public string Title { get; set; }


    }
}

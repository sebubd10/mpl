﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,19,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,19,2021
 * (c) Datavanced LLC
 */

using System;

namespace CoreApi.Models.ViewModels.Auth
{
    public class VMLoginResponse
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool? Admin { get; set; }
        public string DisplayName { get; set; }
        public string Token { get; set; }
        public DateTime LoggedInTime { get; set; }
    }
}

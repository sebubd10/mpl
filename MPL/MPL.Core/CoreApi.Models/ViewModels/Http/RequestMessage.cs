﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,19,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Aug,19,2021
 * (c) Datavanced LLC
 */

namespace CoreApi.Models.ViewModels.Http
{
    public class RequestMessage
    {
        public object ObjectId { get; set; }
        public object RequestObj { get; set; }
    }
}

﻿using AutoMapper;
using CoreApi.Features.CurciteBraker;
using CoreApi.Models.RequestModels.Tickets;
using CoreApi.Models.ViewModels.Http;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoreApi.Features.Ticket
{
    /// <summary>
    /// Create Ticket Command Request works via MediatR
    /// </summary>
    public class CreateTicketCommands : TicketRequest, IRequest<ResponseMessage>
    {
        /// <summary>
        /// Create Ticket Command Handler Fires when the MediatR Request for Create Ticket
        /// </summary>
        public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommands, ResponseMessage>
        {
            private readonly ITicketServie _ticketServie;
            private readonly IMapper _mapper;

            public CreateTicketCommandHandler(ITicketServie ticketServie, IMapper mapper)
            {
                _ticketServie = ticketServie;
                _mapper = mapper;
            }
            /// <summary>
            /// Excutes the Ticket Save Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(CreateTicketCommands command, CancellationToken cancellationToken)
            {
                try
                {
                    var ticket = _mapper.Map<Models.DTO.Tickets.Ticket>(command);
                    return await _ticketServie.Save(ticket);

                }
                catch (Exception ex)
                {

                    throw;
                }

            }


        }
    }
    /// <summary>
    /// Update Ticket Command Request works via MediatR
    /// </summary>
    public class UpdateTicketCommand : TicketRequest, IRequest<ResponseMessage>
    {
        /// <summary>
        /// Update Ticket Command Handler Fires when the MediatR Request for Update Ticket
        /// </summary>
        public class UpdateTicketCommandHandler : IRequestHandler<UpdateTicketCommand, ResponseMessage>
        {
            private readonly ITicketServie _ticketServie;
            private readonly IMapper _mapper;

            public UpdateTicketCommandHandler(ITicketServie ticketServie, IMapper mapper)
            {
                _ticketServie = ticketServie;
                _mapper = mapper;
            }
            /// <summary>
            /// Excutes the Ticket Update Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(UpdateTicketCommand command, CancellationToken cancellationToken)
            {
                var ticket = _mapper.Map<Models.DTO.Tickets.Ticket>(command);
                return await _ticketServie.Update(ticket);
            }
        }
    }
    /// <summary>
    /// Delete Ticket Command Request works via MediatR
    /// </summary>
    public class DeleteTicketCommand : TicketRequest, IRequest<ResponseMessage>
    {
        public int Id { get; set; }
        /// <summary>
        /// Delete Ticket Command Handler Fires when the MediatR Request for Delete Ticket
        /// </summary>
        public class DeleteTicketCommandHandler : IRequestHandler<DeleteTicketCommand, ResponseMessage>
        {
            private readonly ITicketServie _ticketServie;
            public DeleteTicketCommandHandler(ITicketServie ticketServie, IRetryPolicy retryPolicy)
            {
                _ticketServie = ticketServie;
            }
            /// <summary>
            /// Excutes the Ticket Delete Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(DeleteTicketCommand command, CancellationToken cancellationToken)
            {
                var ticket = await _ticketServie.GetDetailsAsync(p => p.Id == command.Id);
                return await _ticketServie.Delete(ticket);
            }
        }
    }
}

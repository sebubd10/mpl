﻿using CoreApi.DAL;
using CoreApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApi.Features.Ticket
{
    public class TicketService : GenericServices<MPLDbContext, Models.DTO.Tickets.Ticket>, ITicketServie
    {
        private readonly IUnitOfWork<MPLDbContext> _unitOfWork;
        public TicketService(IUnitOfWork<MPLDbContext> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
    public interface ITicketServie : IGenericServices<MPLDbContext, Models.DTO.Tickets.Ticket>
    {

    }
}

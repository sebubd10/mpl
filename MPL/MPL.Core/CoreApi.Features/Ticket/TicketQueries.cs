﻿using MediatR;
using CoreApi.Features.CurciteBraker;
using CoreApi.Models.ViewModels.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoreApi.Utils.Enums;

namespace CoreApi.Features.Ticket
{

    /// <summary>
    /// Get all Ticket Query Request works via MediatR
    /// </summary>
    public class GetAllTicketQueries : IRequest<ResponseMessage>
    {
        public Expression<Func<Models.DTO.Tickets.Ticket, bool>> Expression { get; set; }
        public Expression<Func<Models.DTO.Tickets.Ticket, Models.DTO.Tickets.Ticket>> Selector { get; set; }


        /// <summary>
        /// Get All Ticket Query Handler Fires when the MediatR Request for Get all Ticket
        /// </summary>

        public class GetAllTicketQueriesHandler : IRequestHandler<GetAllTicketQueries, ResponseMessage>
        {
            private readonly ITicketServie _ticketServie;
            public GetAllTicketQueriesHandler(ITicketServie ticketServie)
            {
                _ticketServie = ticketServie;
            }
            /// <summary>
            /// Excutes the Get list of tickets Method and also activates the poly retry policy
            /// </summary>
            /// <param name="query"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(GetAllTicketQueries query, CancellationToken cancellationToken)
            {
                ResponseMessage responseMessage = new ResponseMessage();
                try
                {
                    responseMessage.ResponseObj = await _ticketServie.GetListAsync(query.Expression, query.Selector);
                    
                    if (responseMessage.ResponseObj != null)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                    }
                    else
                    {
                        responseMessage.Message = "Data not found";
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                    }

                }
                catch (Exception ex)
                {
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                    responseMessage.Message = ex.Message;
                }

                return responseMessage;
            }
        }
    }
    /// <summary>
    /// Get Details of a Ticket Query Request works via MediatR
    /// </summary>
    public class TicketDetailsQuery : IRequest<ResponseMessage>
    {
        public Expression<Func<Models.DTO.Tickets.Ticket, bool>> Expression { get; set; }
        public Expression<Func<Models.DTO.Tickets.Ticket, Models.DTO.Tickets.Ticket>> Selector { get; set; }

        /// <summary>
        /// Get Details of a Ticket Query Handler Fires when the MediatR Request for Get Details of a Ticket
        /// </summary>
        public class TicketDetailsQueryHandler : IRequestHandler<TicketDetailsQuery, ResponseMessage>
        {

            private readonly ITicketServie _ticketServie;
            public TicketDetailsQueryHandler(ITicketServie ticketServie)
            {
                _ticketServie = ticketServie;
            }
            /// <summary>
            /// Excutes the Get details of a ticket Method and also activates the poly retry policy
            /// </summary>
            /// <param name="query"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(TicketDetailsQuery query, CancellationToken cancellationToken)
            {
                ResponseMessage responseMessage = new ResponseMessage();
                try
                {
                    responseMessage.ResponseObj = await _ticketServie.GetDetailsAsync(query.Expression, query.Selector);
                    if (responseMessage.ResponseObj != null)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                    }
                    else
                    {
                        responseMessage.Message = "Data not found";
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                    }

                }
                catch (Exception ex)
                {
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                    responseMessage.Message = ex.Message;
                }

                return responseMessage;
            }
        }
    }

}

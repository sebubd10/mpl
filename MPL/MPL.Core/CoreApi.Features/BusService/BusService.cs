﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApi.Features.BusService
{
    public class BusService: IBusService
    {
        private readonly IBus _busService;
        public BusService(IBus busService)
        {
            _busService = busService;
        }
        public async Task PublishModel(dynamic product,string endPointPath)
        {
            if (product != null)
            {
                Uri uri = new Uri(endPointPath);
                var endPoint = await _busService.GetSendEndpoint(uri);
                await endPoint.Send(product);
            }
        }
    }

    public interface IBusService
    {
        Task PublishModel(dynamic product, string endPointPath);
    }
}

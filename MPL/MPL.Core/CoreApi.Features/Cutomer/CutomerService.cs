﻿using CoreApi.DAL;
using CoreApi.Models.DTO.Auth;
using CoreApi.Models.DTO.Profile;
using CoreApi.Repository;
using CoreApi.Utils.Constants;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
 
namespace CoreApi.Features.Cutomer
{
    public class CutomerService : GenericServices<MPLDbContext, Models.DTO.Profile.Customer>, ICutomerService
    {
        private readonly IUnitOfWork<MPLDbContext> _unitOfWork;
        private readonly IConfiguration _configuration;
        public CutomerService(IUnitOfWork<MPLDbContext> unitOfWork, IConfiguration configuration) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        public async Task<string> BuildToken(Models.DTO.Profile.Customer user)
        {
            Claim[] claims = null;
            DateTime issuedOn = DateTime.UtcNow;
            var expDate = DateTime.UtcNow.AddMinutes(_configuration.GetValue<int>("Jwt:TokenExpired"));
            claims = GetSystemUserClaims(user, issuedOn, expDate);
            try
            {
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetValue<string>("Jwt:TokenKey")));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityToken(claims: claims, expires: expDate, signingCredentials: creds);
                var tokenValue = new JwtSecurityTokenHandler().WriteToken(token);
                var tokendomain = new AccessToken
                {

                    UserId = user.Id,
                    Token = tokenValue,
                    IssuedOn = issuedOn,
                    ExpiresOn = expDate
                };
                await _unitOfWork.Repository<AccessToken>().InsertAsync(tokendomain);
                if (await _unitOfWork.SaveChangesAsync() > 0)
                {
                    return tokendomain.Token;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }

        }
        private Claim[] GetSystemUserClaims(Customer customer, DateTime issuedOn, DateTime expiredOn)
        {
            var claims = new[] {
                    new Claim(JwtClaims.UserId, customer.Id.ToString()),
                    new Claim(JwtClaims.UserName, customer.Email),
                    //new Claim(JwtClaims.UserTypeId, user.UserTypeId.ToString()),
                    new Claim(JwtClaims.ExpiresDate,expiredOn.ToString(CultureInfo.InvariantCulture)),
                    new Claim(JwtClaims.IssuedOn, issuedOn.ToString(CultureInfo.InvariantCulture)),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
    };
            return claims;
        }
    }
    public interface ICutomerService :  IGenericServices<MPLDbContext, Models.DTO.Profile.Customer>
    {
        Task<string> BuildToken(Models.DTO.Profile.Customer user);
    }
}

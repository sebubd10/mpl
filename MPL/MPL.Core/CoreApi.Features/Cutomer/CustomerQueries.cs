﻿using CoreApi.Features.CurciteBraker;
using CoreApi.Models.ViewModels.Auth;
using CoreApi.Models.ViewModels.Http;
using CoreApi.Utils.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoreApi.Features.Cutomer
{
    /// <summary>
    /// Get all Cutomer Query Request works via MediatR
    /// </summary>
    public class GetAllCustomerQueries : IRequest<ResponseMessage>
    {
        public Expression<Func<Models.DTO.Profile.Customer, bool>> Expression { get; set; }
        public Expression<Func<Models.DTO.Profile.Customer, Models.DTO.Profile.Customer>> Selector { get; set; }
        /// <summary>
        /// Get All Customer Query Handler Fires when the MediatR Request for Get all customer
        /// </summary>
        public class GetAllCustomerQueriesHandler : IRequestHandler<GetAllCustomerQueries, ResponseMessage>
        {
            private readonly ICutomerService _cutomerService;
            public GetAllCustomerQueriesHandler(ICutomerService cutomerService)
            {
                _cutomerService = cutomerService;
            }
            /// <summary>
            /// Excutes the Get list of customers Method and also activates the poly retry policy
            /// </summary>
            /// <param name="query"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(GetAllCustomerQueries query, CancellationToken cancellationToken)
            {
                ResponseMessage responseMessage = new ResponseMessage();
                try
                {

                    responseMessage.ResponseObj = await _cutomerService.GetListAsync(query.Expression, query.Selector);
                    if (responseMessage.ResponseObj != null)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                    }
                    else
                    {
                        responseMessage.ResponseObj = null;
                        responseMessage.Message = "Data not found.";
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                    }

                }
                catch (Exception ex)
                {
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                    responseMessage.ResponseObj = null;
                    responseMessage.Message = ex.Message;
                }

                return responseMessage;
            }
        }


    }
    /// <summary>
    /// Get Details of a Cutomer Query Request works via MediatR
    /// </summary>


    public class CustomerDetailsQuery : IRequest<ResponseMessage>
    {

        public Expression<Func<Models.DTO.Profile.Customer, bool>> Expression { get; set; }
        public Expression<Func<Models.DTO.Profile.Customer, Models.DTO.Profile.Customer>> Selector { get; set; }


        public class CustomerDetailsQueryHandler : IRequestHandler<CustomerDetailsQuery, ResponseMessage>
        {

            private readonly ICutomerService _cutomerService;
            public CustomerDetailsQueryHandler(ICutomerService cutomerService)
            {
                _cutomerService = cutomerService;
            }
            public async Task<ResponseMessage> Handle(CustomerDetailsQuery query, CancellationToken cancellationToken)
            {
                ResponseMessage responseMessage = new ResponseMessage();
                try
                {
                    responseMessage.ResponseObj = await _cutomerService.GetDetailsAsync(query.Expression, query.Selector);

                    if (responseMessage.ResponseObj != null)
                    {
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                    }
                    else
                    {
                        responseMessage.ResponseObj = null;
                        responseMessage.Message = "Data not found.";
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                    }

                }
                catch (Exception ex)
                {
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                    responseMessage.ResponseObj = null;
                    responseMessage.Message = ex.Message;
                }

                return responseMessage;
            }

        }
    }



    public class CustomerAuthQuery : IRequest<ResponseMessage>
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public class CustomerAuthQueryHandler : IRequestHandler<CustomerAuthQuery, ResponseMessage>
        {

            private readonly ICutomerService _cutomerService;

            public CustomerAuthQueryHandler(ICutomerService cutomerService)
            {
                _cutomerService = cutomerService;
            }
            public async Task<ResponseMessage> Handle(CustomerAuthQuery query, CancellationToken cancellationToken)
            {
                ResponseMessage responseMessage = new ResponseMessage();
                try
                {

                    var result = await _cutomerService.GetDetailsAsync(p => p.Email == query.Email && p.Password == query.Password, q => q);
                    if (result != null)
                    {
                        var token = await _cutomerService.BuildToken(result);

                        var loginVm = new VMLoginResponse()
                        {
                            UserId = result.Id,
                            Admin = false,
                            DisplayName = result.FullName,
                            LoggedInTime = DateTime.Now,
                            Token = token,
                            UserName = result.Email

                        };

                        responseMessage.ResponseObj = loginVm;
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                    }
                    else
                    {
                        responseMessage.Message = "Incorrect Email/Password";
                        responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Failed;
                    }

                }
                catch (Exception ex)
                {
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                    responseMessage.Message = ex.Message;
                }

                return responseMessage;
            }
        }
    }
}

﻿using AutoMapper;
using CoreApi.Features.CurciteBraker;
using CoreApi.Models.DTO.Profile;
using CoreApi.Models.RequestModels.Profile;
using CoreApi.Models.ViewModels.Http;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoreApi.Features.Cutomer
{
    /// <summary>
    /// Create Cutomer Command Request works via MediatR
    /// </summary>
    public class CreateCutomerCommand : CustomerRequest, IRequest<ResponseMessage>
    {
        /// <summary>
        /// Create Customer Command Handler Fires when the MediatR Request for Create Customer
        /// </summary>
        public class CreateCutomerCommandHandler : IRequestHandler<CreateCutomerCommand, ResponseMessage>
        {
            private readonly ICutomerService _cutomerService;
            private readonly IMapper _mapper;
            //readonly IMapper<CustomerRequest, Models.DTO.Profile.Customer> mapper;
            public CreateCutomerCommandHandler(ICutomerService cutomerService, IMapper mapper)
            {
                _cutomerService = cutomerService;
                _mapper = mapper;
            }
            /// <summary>
            /// Excutes the Customer Save Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(CreateCutomerCommand command, CancellationToken cancellationToken)
            {
                try
                {
                    var customer = _mapper.Map<Customer>(command);
                    return await _cutomerService.Save(customer);
                }
                catch (Exception ex)
                {

                    throw;
                }

            }
        }
    }
    /// <summary>
    /// Update Cutomer Command Request works via MediatR
    /// </summary>
    public class UpdateCustomerCommand : CustomerRequest, IRequest<ResponseMessage>
    {
        /// <summary>
        /// Update Customer Command Handler Fires when the MediatR Request for update Customer
        /// </summary>
        public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, ResponseMessage>
        {
            private readonly ICutomerService _cutomerService;
            private readonly IMapper _mapper;

            public UpdateCustomerCommandHandler(ICutomerService cutomerService, IMapper mapper)
            {
                _cutomerService = cutomerService;
                _mapper = mapper;
            }
            /// <summary>
            /// Excutes the Customer Update Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(UpdateCustomerCommand command, CancellationToken cancellationToken)
            {
                var customer = _mapper.Map<Customer>(command);
                return await _cutomerService.Update(customer);
            }
        }
    }
    /// <summary>
    /// Delete Cutomer Command Request works via MediatR
    /// </summary>
    public class DeleteCustomerCommand : IRequest<ResponseMessage>
    {
        public int Id { get; set; }

        /// <summary>
        /// Delete Customer Command Handler Fires when the MediatR Request for delete Customer
        /// </summary>
        public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, ResponseMessage>
        {
            private readonly ICutomerService _cutomerService;
            public DeleteCustomerCommandHandler(ICutomerService cutomerService)
            {
                _cutomerService = cutomerService;
            }
            /// <summary>
            /// Excutes the Customer Delete Method and also activates the poly retry policy
            /// </summary>
            /// <param name="command"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            public async Task<ResponseMessage> Handle(DeleteCustomerCommand command, CancellationToken cancellationToken)
            {
                var customer = await _cutomerService.GetDetailsAsync(p => p.Id == command.Id);
                return await _cutomerService.Delete(customer);
            }
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CoreApi.Features.Auth.UserType;
using CoreApi.Features.BusService;
using CoreApi.Features.CurciteBraker;
using CoreApi.Features.Cutomer;
using CoreApi.Features.Ticket;

namespace CoreApi.Features
{
    public static class FeaturesDependencyRegistrar
    {
        /// <summary>
        /// Register Repository and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        public static void FeaturesDependencyResolver(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICutomerService, CutomerService>();
            services.AddTransient<ITicketServie, TicketService>();
            services.AddTransient<IUserTypeService, UserTypeService>();
            services.AddTransient<IRetryPolicy, RetryPolicy>();
            services.AddTransient<IBusService, CoreApi.Features.BusService.BusService>();
        }
    }

}

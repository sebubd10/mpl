﻿using CoreApi.Models.RequestModels.Auth;
using CoreApi.Models.ViewModels.Http;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoreApi.Features.Auth.UserType
{
    public class CreateUserTypeCommand : UserTypeRequest,  IRequest<ResponseMessage>
    {
        public class CreateUserTypeCommandHandler : IRequestHandler<CreateUserTypeCommand, ResponseMessage>
        {
            private readonly IUserTypeService _userTypeService;
            public CreateUserTypeCommandHandler(IUserTypeService userTypeService)
            {
                _userTypeService = userTypeService;
            }
            public async Task<ResponseMessage> Handle(CreateUserTypeCommand command, CancellationToken cancellationToken)
            {
                try
                {
                    //var userType = this.mapper.Map(command);
                    return await _userTypeService.Save(null);
                }
                catch (Exception ex)
                {

                    throw;
                }
                
            }
        }
    }

    public class UpdateUserTypeCommand : UserTypeRequest, IRequest<ResponseMessage>
    {
        public class UpdateUserTypeCommandHandler : IRequestHandler<UpdateUserTypeCommand, ResponseMessage>
        {
            private readonly IUserTypeService _userTypeService;
            public UpdateUserTypeCommandHandler(IUserTypeService userTypeService)
            {
                _userTypeService = userTypeService;
            }
            public async Task<ResponseMessage> Handle(UpdateUserTypeCommand command, CancellationToken cancellationToken)
            {
                return await _userTypeService.Update(null);
            }
        }
    }

    public class DeleteUserTypeCommand : UserTypeRequest, IRequest<ResponseMessage>
    {
        public class DeleteUserTypeCommandHandler : IRequestHandler<DeleteUserTypeCommand, ResponseMessage>
        {
            private readonly IUserTypeService _userTypeService;
            public DeleteUserTypeCommandHandler(IUserTypeService userTypeService)
            {
                _userTypeService = userTypeService;
            }
            public async Task<ResponseMessage> Handle(DeleteUserTypeCommand command, CancellationToken cancellationToken)
            {
                return await _userTypeService.Delete(null);
            }
        }
    }
}

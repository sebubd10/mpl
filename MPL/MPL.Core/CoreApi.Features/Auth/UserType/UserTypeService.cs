﻿using CoreApi.DAL;
using CoreApi.Models.ViewModels.Http;
using CoreApi.Repository;
using CoreApi.Utils.Enums;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CoreApi.Features.Auth.UserType
{
    public class UserTypeService : GenericServices<MPLDbContext, Models.DTO.Auth.UserType>, IUserTypeService
    {
        private readonly IUnitOfWork<MPLDbContext> _unitOfWork;
        public UserTypeService(IUnitOfWork<MPLDbContext> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseMessage> Save(Models.DTO.Auth.UserType model)
        {
            try
            {
                if (await _unitOfWork.Repository<Models.DTO.Auth.UserType>()
                    .IsExistAsync(x => x.UserTypeName == model.UserTypeName
                        && x.Status != (int)AppEnums.Status.Deleted))
                {
                    return new ResponseMessage
                    {
                        ResponseCode = (int)AppEnums.ResponseCode.Failed,
                        Message = $"User Type: {model.UserTypeName} Already Exist !"
                    };
                }
                await _unitOfWork.Repository<Models.DTO.Auth.UserType>().InsertAsync(model);
                if (await _unitOfWork.SaveChangesAsync() > 0)
                {
                    return new ResponseMessage
                    {
                        ResponseCode = (int)AppEnums.ResponseCode.Success,
                        Message = $"User Type: {model.UserTypeName} Created !",
                        ResponseObj = model
                    };
                }
                else
                {
                    return new ResponseMessage
                    {
                        ResponseCode = (int)AppEnums.ResponseCode.Failed,
                        Message = $"Database failed to save user type: {model.UserTypeName}."
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    ResponseCode = (int)AppEnums.ResponseCode.InternalServerError,
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseMessage> Update(Models.DTO.Auth.UserType model)
        {
            try
            {
                _unitOfWork.Repository<Models.DTO.Auth.UserType>().Update(model);

                if (await _unitOfWork.Repository<Models.DTO.Auth.UserType>()
                    .IsExistAsync(x => x.UserTypeName == model.UserTypeName
                        && x.Status != (int)AppEnums.Status.Deleted))
                {
                    _unitOfWork.DbContext.Entry(model).Property(x => x.UserTypeName).IsModified = false;
                }
                _unitOfWork.DbContext.Entry(model).Property(x => x.CreatedBy).IsModified = false;
                _unitOfWork.DbContext.Entry(model).Property(x => x.CreatedDate).IsModified = false;

                if (await _unitOfWork.SaveChangesAsync() > 0)
                {
                    return new ResponseMessage
                    {
                        ResponseCode = (int)AppEnums.ResponseCode.Success,
                        Message = $"User Type: {model.UserTypeName} Updated !",
                        ResponseObj = model
                    };
                }
                else
                {
                    return new ResponseMessage
                    {
                        ResponseCode = (int)AppEnums.ResponseCode.Failed,
                        Message = $"Database failed to update user type: {model.UserTypeName}."
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    ResponseCode = (int)AppEnums.ResponseCode.InternalServerError,
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseMessage> Delete(Models.DTO.Auth.UserType model)
        {
            try
            {
                _unitOfWork.Repository<Models.DTO.Auth.UserType>().Delete(model);
                if (await _unitOfWork.SaveChangesAsync() > 0)
                {
                    return new ResponseMessage
                    {
                        ResponseCode = (int)AppEnums.ResponseCode.Success,
                        Message = $"User Type: {model.UserTypeName} Deleted !",
                        ResponseObj = model
                    };
                }
                else
                {
                    return new ResponseMessage
                    {
                        ResponseCode = (int)AppEnums.ResponseCode.Failed,
                        Message = $"Database failed to delete user type: {model.UserTypeName}."
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    ResponseCode = (int)AppEnums.ResponseCode.InternalServerError,
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseMessage> Details(Expression<Func<Models.DTO.Auth.UserType, bool>> expression)
        {
            try
            {
                //for (int i = 0; i < 1000; i++)
                //{
                //    await _unitOfWork.Repository<UserType>().GetFirstOrDefaultAsync(predicate: expression);
                //}
                return new ResponseMessage
                {
                    ResponseCode = (int)AppEnums.ResponseCode.Success,
                    Message = "Success !",
                    ResponseObj = await _unitOfWork.Repository<Models.DTO.Auth.UserType>().GetFirstOrDefaultAsync(predicate: expression)
                };

            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    ResponseCode = (int)AppEnums.ResponseCode.InternalServerError,
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseMessage> List(Expression<Func<Models.DTO.Auth.UserType, bool>> expression)
        {
            try
            {
                return new ResponseMessage
                {
                    ResponseCode = (int)AppEnums.ResponseCode.Success,
                    Message = "Success !",
                    ResponseObj = await _unitOfWork.Repository<Models.DTO.Auth.UserType>().GetAsync(predicate: expression)
                };
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    ResponseCode = (int)AppEnums.ResponseCode.InternalServerError,
                    Message = ex.Message
                };
            }
        }

        private bool _disposed = false;

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public interface IUserTypeService : IGenericServices<MPLDbContext, Models.DTO.Auth.UserType>
    {
        Task<ResponseMessage> Save(Models.DTO.Auth.UserType model);
        Task<ResponseMessage> Update(Models.DTO.Auth.UserType model);
        Task<ResponseMessage> Delete(Models.DTO.Auth.UserType model);
        Task<ResponseMessage> Details(Expression<Func<Models.DTO.Auth.UserType, bool>> expression);
        Task<ResponseMessage> List(Expression<Func<Models.DTO.Auth.UserType, bool>> expression);
    }
}

﻿using CoreApi.Models.ViewModels.Http;
using CoreApi.Utils.Enums;
using MediatR;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CoreApi.Features.Auth.UserType
{
    public class GetAllUserTypeQuery : IRequest<ResponseMessage>
    {
        public Expression<Func<Models.DTO.Auth.UserType, bool>> Expression { get; set; }
        public Expression<Func<Models.DTO.Auth.UserType, Models.DTO.Auth.UserType>> Selector { get; set; }
        public class GetAllProductsQueryHandler : IRequestHandler<GetAllUserTypeQuery, ResponseMessage>
        {
            private readonly IUserTypeService _userTypeService;
            public GetAllProductsQueryHandler(IUserTypeService userTypeService)
            {
                _userTypeService = userTypeService;
            }
            public async Task<ResponseMessage> Handle(GetAllUserTypeQuery query, CancellationToken cancellationToken)
            {
                ResponseMessage responseMessage = new ResponseMessage();
                try
                {
                    responseMessage.ResponseObj = await _userTypeService.GetListAsync(query.Expression, query.Selector);
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.Success;
                }
                catch (Exception ex)
                {
                    responseMessage.ResponseCode = (int)AppEnums.ResponseCode.InternalServerError;
                    responseMessage.ResponseObj = null;
                }

                return responseMessage;
            }
        }
    }

    public class UserTypesDetailsQuery : IRequest<Models.DTO.Auth.UserType>
    {
        public Expression<Func<Models.DTO.Auth.UserType, bool>> Expression { get; set; }
        public Expression<Func<Models.DTO.Auth.UserType, Models.DTO.Auth.UserType>> Selector { get; set; }

    }

    public class UserTypesDetailsQueryHandler : IRequestHandler<UserTypesDetailsQuery, Models.DTO.Auth.UserType>
    {
        private readonly IUserTypeService _userTypeService;
        public UserTypesDetailsQueryHandler(IUserTypeService userTypeService)
        {
            _userTypeService = userTypeService;
        }
        public async Task<Models.DTO.Auth.UserType> Handle(UserTypesDetailsQuery query, CancellationToken cancellationToken)
        {
            return await _userTypeService.GetDetailsAsync(query.Expression, query.Selector);
        }
    }
}

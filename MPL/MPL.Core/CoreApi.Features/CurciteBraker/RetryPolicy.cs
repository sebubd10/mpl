﻿using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApi.Features.CurciteBraker
{
    /// <summary>
    /// Poly Circuite Braker Retry Policy Class
    /// </summary>
    public class RetryPolicy : IRetryPolicy
    {
        public RetryPolicy()
        {
           
        }
        /// <summary>
        /// Returns the poly curcite braker retry policy instance against the rerty count and time span
        /// </summary>
        /// <param name="retryCount"></param>
        /// <param name="timeSpan"></param>
        /// <returns></returns>
        public AsyncRetryPolicy GetRetryPolicy(int retryCount , TimeSpan timeSpan)
        {
            AsyncRetryPolicy retryPolicy = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(retryCount, retryAttempt => {
                    var timeToWait = timeSpan;
                    return timeToWait;
                });

            return retryPolicy;
        }

    }
}

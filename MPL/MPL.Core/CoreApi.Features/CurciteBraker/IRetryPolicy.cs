﻿using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApi.Features.CurciteBraker
{
    public interface IRetryPolicy
    {
        AsyncRetryPolicy GetRetryPolicy(int retryCount, TimeSpan timeSpan);
    }
}

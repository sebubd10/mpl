﻿/*
 * Created By  	: Md. Mozaffar Rahman Sebu
 * Created Date	: Aug,19,2021
 * Updated By  	: Md. Mozaffar Rahman Sebu
 * Updated Date	: Sep,13,2021
 * (c) Datavanced LLC
 */

namespace CoreApi.Utils.Constants
{
    public class SmartSheetCareGiversConstants
    {
        public const string Created = "created";
        public const string Modified = "modified";
        public const string AssignedTo = "assigned to";
        public const string Status = "status";
        public const string PatientName = "patient name";
        public const string FollowUp = "follow up";
        public const string CaregiverName = "caregiver name"; 
        public const string CaregiverNumber = "caregiver's number"; 
        public const string Address = "address";
        public const string City = "city";
        public const string Zip = "zip code";
    }

    public class SmartSheetPatientConstants
    {
        public const string DATE_CREATED = "Date Created";
        public const string NAME = "Name";
        public const string NUMBERS = "Numbers";
        public const string PHONE_NUMBER_2 = "Phone Number 2";
        public const string NOTES_SCHEDULE = "Notes/Schedule";
        public const string STATUS = "Status";
        public const string AUTH_DATE = "Auth Date"; 
        public const string ACTIVELY_FINDING_AIDE = "Actively finding aide"; 
        public const string ASSIGNED_TO = "Assigned To";
        public const string STATUS_DETAILS = "Status Details";
        public const string CREATED_BY = "Created By";
        public const string AGENCY = "Agency";
        public const string SOURCE = "Source";
        public const string MODIFIED = "Modified";
        public const string NOTES = "Notes";
        public const string DOB = "DOB";
        public const string CONTACT = "Contact";
        public const string NUMBER = "Number";
        public const string SERVICE_AUTH = "Service Auth";
        public const string ADDRESS = "Address";
        public const string CITY = "City";
        public const string ZIP_CODE = "Zip Code";
        public const string COUNTY = "County";
    }
}

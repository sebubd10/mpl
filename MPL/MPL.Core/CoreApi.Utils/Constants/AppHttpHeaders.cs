﻿namespace CoreApi.Utils.Constants
{
    public class AppHttpHeaders
    {
        public const string Token = "Authorization";
        public const string AuthenticationSchema = "Bearer ";
    }
}

using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MPL.Scheduler.Context;
using MPL.Scheduler.Service;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;


namespace MPL.Scheduler
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IServiceProvider _serviceProvider;
        
        public Worker(ILogger<Worker> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            foreach(var data in new TaskList().Scheduling())
            {
                if (data.Type.Equals("Query")){
                    RecurringJob.AddOrUpdate(data.Id.ToString()/*"easyjob"*/, () => MethodInitialization("IQueryService", 
                        "ExecuteStoreProcedure" , new object[] { data.StoreProcedure }), data.CornExpression);
                }
                else if (data.Type.Equals("Method"))
                {
                    RecurringJob.AddOrUpdate(data.Id.ToString()/*"easyjob"*/, () => MethodInitialization(data.InterfaceName, 
                        data.MethodName , null), data.CornExpression);
                }
            }
            
        }
        public void MethodInitialization(string interfaceName , string methodName , object[] parameters)
        {
            var type = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                        from t in assembly.GetTypes()
                        where t.Name == interfaceName
                        select t).FirstOrDefault();

            if (type == null)
                throw new InvalidOperationException("Type not found");

            var service = _serviceProvider.GetService(type);
            MethodInfo methodInfo = type.GetMethod(methodName);
            methodInfo.Invoke(service, parameters);
        }


    }
}



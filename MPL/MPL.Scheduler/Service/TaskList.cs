﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Scheduler.Service
{
    public class TaskList
    {
        public List<SchdulingTask> Scheduling()
        {
            var task = new SchdulingTask()
            {
                Id = 1,
                InterfaceName = "IQueryService",
                MethodName = "ExecuteStoreProcedure",
                StoreProcedure = "exec CreateCustomer",
                CornExpression = "*/1 * * * *",
                Type = "Query"
            };

            var task1 = new SchdulingTask()
            {
                Id = 2,
                InterfaceName = "IJobService",
                MethodName = "CreateCustomer",
                StoreProcedure = null,
                CornExpression = "*/5 * * * *",
                Type = "Method"
            };
            List<SchdulingTask> schdulingTasks = new List<SchdulingTask>();
            schdulingTasks.Add(task);
            schdulingTasks.Add(task1);
            return schdulingTasks;
        }
    }
}
public class SchdulingTask
{
    public int Id { set; get; }
    public string InterfaceName { set; get; }
    public string MethodName { set; get; }
    public string StoreProcedure { set; get; }
    public string CornExpression { set; get; }
    public string Type { set; get; }
   
}

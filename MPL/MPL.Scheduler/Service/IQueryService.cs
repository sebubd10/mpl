﻿using MPL.Scheduler.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Scheduler.Service
{
    public interface IQueryService
    {
        public void ExecuteStoreProcedure(string StoreProcedure);
    }
    public class QueryService : IQueryService
    {
        private readonly DatabaseContext _databaseContext;

        public QueryService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public void ExecuteStoreProcedure(string StoreProcedure)
        {
            _databaseContext.ExecuteSqlCommand(StoreProcedure);
        }
    }
}

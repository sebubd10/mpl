﻿using Microsoft.Extensions.Logging;
using MPL.Scheduler.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Scheduler.Service
{
    public interface IJobService
    {
        void FireAndForgetJob();

        void ReccuringJob();

        void DelayedJob(string prams);

        void ContinuationJob();

        void CreateCustomer();

    }
    
    public class JobService  : IJobService
    {
        private readonly ILogger<Worker> _logger;
        private readonly DatabaseContext _databaseContext;
        public JobService(ILogger<Worker> logger, DatabaseContext databaseContext)
        {
            _logger = logger;
            _databaseContext = databaseContext;
        }
        public void FireAndForgetJob()
        {
            var writeFile = "Hello FireAndForgetJob";
            //string path= "/FireAndForgetJob"
            //    //if()
            //Directory.CreateDirectory()
            //File.WriteAllText(dire,writeFile)
        }

        public void ReccuringJob()
        {
            Console.WriteLine("Hello from a Scheduled job!");
        }

        public void DelayedJob(string parms)
        {
            Console.WriteLine("Hello from a Delayed job!");
        }

        public void ContinuationJob()
        {
            Console.WriteLine("Hello from a Continuation job!");
        }

        public void CreateCustomer()
        {
            string FulName = RandomString(7);
            string Address = RandomString(15);
            string Email = "Method@" + RandomString(5) + ".yopmail.com";
            string MobileNumber = RandomString(11, "0123456789");
            string Password = RandomString(9);
            DateTime dateTime = DateTime.UtcNow;
            string query = string.Format("insert into CustomCustomers" +
                "(FullName,Address,Email,MobileNumber,Password,DateTime) values" +
                "('{0}' , '{1}' , '{2}' , '{3}' , '{4}' , '{5}')", 
                FulName, Address, Email, MobileNumber, Password, dateTime);
            _databaseContext.ExecuteSqlCommand(query);
        }

        private string RandomString(int length, string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        {
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Doc.Channel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public IActionResult Get()
        {
			using (System.Net.WebClient wc = new System.Net.WebClient())
			{
				wc.Headers.Add("Cookie: Authentication=user"); // add a cookie header to the request
				try
				{
					string filename = System.Guid.NewGuid().ToString("N"); // use global unique identifier for file name to avoid conflicts
					wc.DownloadFile("http://www.prowaretech.com/", System.IO.Path.DirectorySeparatorChar + "files" + System.IO.Path.DirectorySeparatorChar + filename); // could add a file extension here
																																										// do something with file
				}
				catch (System.Exception ex)
				{
					// check exception object for the error
				}
			}
			return Ok();
		}
    }
}

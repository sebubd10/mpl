﻿using MPL.Doc.Channel.Models;
using MPL.Doc.Channel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Doc.Channel.Features
{
    public class PolicyFeature : IPolicyFeature
    {
        public IPolicyService _policyService;

        public PolicyFeature(IPolicyService policyService)
        {
            _policyService = policyService;
        }
        public List<PolicyFeatureModel> GetPolicyDocuments(PolicyFeatureRequestModel requestModel)
        {
            var data = _policyService.GetPolicyDocuments(requestModel.PolicyNumber, requestModel.DocumentType)
                .Select(x => new { x.Id, x.Name });

            var downLoadList = _policyService.DownloadPolicyDocuments(data.Select(x => x.Id).ToArray());

            var result = (from a in downLoadList
                         join v in data on
                         a equals v.Id
                         select new PolicyFeatureModel
                         {
                             Id = a,
                             Name = v.Name

                         }).ToList();

            return result;
        }
    }
    public interface IPolicyFeature
    {
        public List<PolicyFeatureModel> GetPolicyDocuments(PolicyFeatureRequestModel requestModel);
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Doc.Channel.Models
{
    public class PolicyFeatureModel
    {
        public string Id { set; get; }
        public string Name { set; get; }
    }
}

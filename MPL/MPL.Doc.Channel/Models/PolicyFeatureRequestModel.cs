﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Doc.Channel.Models
{
    public class PolicyFeatureRequestModel
    {
        public string PolicyNumber { set; get; }
        public string DocumentType { set; get; }
    }
}

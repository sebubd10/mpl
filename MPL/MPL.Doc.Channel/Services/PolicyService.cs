﻿using MPL.Doc.Channel.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPL.Doc.Channel.Services
{
    public class PolicyService: IPolicyService
    {
        public List<Document> GetPolicyDocuments(string policyNumber,string documentType)
        {
            try
            {
                var client = new RestClient($"https://bdclaimqa.metlife.com.bd/public/filenet/api/adhoc?policyNumber={policyNumber}&documentType={documentType}");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                return JsonConvert.DeserializeObject<List<Document>>(response.Content);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<string> DownloadPolicyDocuments(params string[] policyNumbers)
        {
            try
            {
                foreach (var item in policyNumbers)
                {
                    //var client = new RestClient($"https://bdclaimqa.metlife.com.bd/public/filenet/api/document?id={item}&documentType=PolicyDocument");
                    //client.Timeout = -1;
                    //var request = new RestRequest(Method.GET);
                    //IRestResponse response = client.Execute(request);
                }
                
                return policyNumbers.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public interface IPolicyService
    {
        List<Document> GetPolicyDocuments(string policyNumber, string documentType);
        List<string> DownloadPolicyDocuments(params string[] policyNumbers);
    }
}
